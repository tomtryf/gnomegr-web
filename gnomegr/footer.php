    </div> <!-- END gnome-content -->

<!-- footer -->
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="links col-xs-12 col-sm-9">
                        <?php
                            wp_nav_menu('menu=footer');
                        ?>
                    </div>
                    <div class="icons col-xs-12 col-sm-3">
                        <?php require_once("social_icons.php"); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- footnotes -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="footnotes" class="col-sm-9">
                        <strong class="gnome_logo">&copy; <?php echo date("Y"); ?> <?php echo get_theme_mod('footer_copyright_text');?></strong><br />
                        <small>
                            <?php echo get_theme_mod('footer_notes');?>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php wp_footer(); ?>
    </body>
</html>
