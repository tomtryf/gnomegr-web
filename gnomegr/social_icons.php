    <div class="social_network_icons">
        <a href="https://gnome.gr/feed" target="_blank" aria-label="RSS Feed"><i class="fa fa-2x fa-rss" aria-hidden="true" title="RSS Feed"></i></a>
        <a href="https://www.facebook.com/gnomegr" target="_blank" aria-label="GNOMEGR on Facebook"><i class="fa fa-2x fa-facebook" aria-hidden="true" title="GNOMEGR on Facebook"></i></a>
        <a href="https://twitter.com/gnomegr" target="_blank" aria-label="GNOMEGR on Twitter"><i class="fa fa-2x fa-twitter" aria-hidden="true" title="GNOMEGR on Twitter"></i></a>
        <a href="https://plus.google.com/+GnomeGr" target="_blank" aria-label="GNOMEGR on Google Plus"><i class="fa fa-2x fa-google-plus" aria-hidden="true" title="GNOMEGR on Google Plus"></i></a>
    </div>
